from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from . import models
from .serializers import HomeWorkSerializer, FinishedHomeWorkSerializer


class HomeWorkAPIView(APIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = HomeWorkSerializer

    def get(self, request):
        queryset = models.HomeWork.objects.all()
        serializer = HomeWorkSerializer(queryset, many=True)
        return Response(serializer.data)


class FinishedHomeWorkAPIView(APIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = FinishedHomeWorkSerializer

    def get(self, request):
        queryset = models.FinishedHomeWork.objects.all()
        serializer = FinishedHomeWorkSerializer(queryset, many=True)
        return Response(serializer.data)

